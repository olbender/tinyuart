## tinyuart: a minimalistic header-only C++ library for UART communication in Linux

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

tinyuart is a small and efficient header-only library written in 
modern C++ to provide functionality UART communication.

## Table of Contents
* [Dependencies](#dependencies)
* [License](#license)

## Dependencies
No dependencies! All you need is a C++14-compliant compiler as the project ships the following dependencies as part of the source distribution:

* [Unit Test Framework Catch2](https://github.com/catchorg/Catch2/releases/tag/v2.1.1) - [![License: Boost Software License v1.0](https://img.shields.io/badge/License-Boost%20v1-blue.svg)](http://www.boost.org/LICENSE_1_0.txt) - [Source](https://github.com/olbender/tinyso/blob/master/test/catch.hpp)

## License
* This project is released under the terms of the MIT License - [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
