/*
 * MIT License
 *
 * Copyright (c) 2018 Ola Benderius
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TINYUART_HPP
#define TINYUART_HPP

#include <sys/epoll.h>

#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace tinyuart {

class Uart {
 public:
  Uart();
  virtual ~Uart();
  bool connect(std::string const &, uint32_t);
  void listen(std::function<void(std::string const &)>);
  void stop();
  bool write(std::string const &);

 private:
  bool m_running;
  int32_t m_fd;
};

inline Uart::Uart():
  m_running(false),
  m_fd(-1)
{
}

inline Uart::~Uart()
{
  ::close(m_fd);
}

inline bool Uart::connect(std::string const &port, uint32_t speed)
{
  m_fd = ::open(port.c_str(), O_RDWR);
  if (m_fd == -1) {
    std::cerr << "Could not open serial port '" << port << "'." << std::endl;
    return false;
  }

  uint32_t speed_valid{B0};
  if (speed == 50) {
    speed_valid = B50;
  } else if (speed == 75) {
    speed_valid = B75;
  } else if (speed == 110) {
    speed_valid = B110;
  } else if (speed == 134) {
    speed_valid = B134;
  } else if (speed == 150) {
    speed_valid = B150;
  } else if (speed == 200) {
    speed_valid = B200;
  } else if (speed == 300) {
    speed_valid = B300;
  } else if (speed == 600) {
    speed_valid = B600;
  } else if (speed == 1200) {
    speed_valid = B1200;
  } else if (speed == 1800) {
    speed_valid = B1800;
  } else if (speed == 2400) {
    speed_valid = B2400;
  } else if (speed == 4800) {
    speed_valid = B4800;
  } else if (speed == 9600) {
    speed_valid = B9600;
  } else if (speed == 19200) {
    speed_valid = B19200;
  } else if (speed == 38400) {
    speed_valid = B38400;
  } else if (speed == 57600) {
    speed_valid = B57600;
  } else if (speed == 115200) {
    speed_valid = B115200;
  } else if (speed == 230400) {
    speed_valid = B230400;
  } else {
    std::cerr << "Invalid speed '" << speed << "'." << std::endl;
    ::close(m_fd);
    return false;
  }

  struct termios opt;
  ::tcgetattr(m_fd, &opt);

  ::cfsetispeed(&opt, speed_valid);
  ::cfsetospeed(&opt, speed_valid);

  opt.c_lflag  &= ~(ICANON | ECHO | ECHOE | ISIG);
  opt.c_oflag  &= ~OPOST;

  // TODO: Set data, parity, and stop bits?

  ::tcsetattr(m_fd, TCSANOW, &opt);

  return true;
}

inline void Uart::listen(std::function<void(std::string const &)> decode)
{
  int32_t efd = epoll_create1(0);

  struct epoll_event event;
  event.data.fd = m_fd;
  event.events = EPOLLIN | EPOLLET;

  epoll_ctl(efd, EPOLL_CTL_ADD, m_fd, &event);

  uint32_t const max_events{64};
  struct epoll_event events[max_events];

  char buff[512];

  m_running = true;
  while (m_running) {
    uint32_t max_wait_ms{1000};
    int32_t nfds = epoll_wait(efd, events, max_events, max_wait_ms);

    for (int32_t n{0}; n < nfds; ++n) {
      uint32_t length = read(events[n].data.fd, buff, sizeof(buff));
      if (length > 0) {
        std::string input(buff, length);
        decode(input);
      }
    }
  }
}

inline void Uart::stop()
{
  m_running = false;
}

inline bool Uart::write(std::string const &output)
{
  uint32_t n = ::write(m_fd, output.c_str(), output.length());
  if (n != output.length()) {
    std::cerr << "Could not write to port." << std::endl;
    return false;
  }
  return true;
}

}

#endif
