/*
 * MIT License
 *
 * Copyright (c) 2020 Ola Benderius
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <thread>
#include <chrono>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "tinyuart.hpp"

TEST_CASE("Test with virtual ports")
{
  std::cout << "The test assumes that there is a virtual serial cable such as: "
    "'socat -d -d pty,raw,echo=0 pty,raw,echo=0' where the two ends of the "
    "connection are named '/dev/pts/10' and '/dev/pts/11'." << std::endl;

  uint32_t speed{115200};
  
  std::string text = "Some nice test data to be sent";
  
  // Set up receiver
  std::string port0{"/dev/pts/10"};
  tinyuart::Uart uart0;

  REQUIRE(uart0.connect(port0, speed));

  auto runner{[&uart0, &text]() {
    auto decode = [&text](std::string data)
    {
      REQUIRE(data == text);
    };

    uart0.listen(decode);
  }};
  
  std::thread uart_thread = std::thread(runner);
  
  // Set up sender
  std::string port1{"/dev/pts/11"};
  tinyuart::Uart uart1;
  
  REQUIRE(uart1.connect(port1, speed));
  
  for (uint32_t i{0}; i < 3; i++) {
    REQUIRE(uart1.write(text));
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }
  
  uart0.stop();
  uart1.stop();

  uart_thread.join();
  REQUIRE(true);
}
